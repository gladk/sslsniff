From: Giovanni Mascellani <gio@debian.org>
Date: Sat, 2 May 2020 11:24:55 +0200
Subject: Fix FTBFS with Boost 1.71.

---
 RawBridge.hpp                  | 21 +++++++++++++++++++++
 SSLConnectionManager.cpp       |  6 +++---
 http/HttpBridge.hpp            | 25 +++++++++++++++++++++++++
 http/HttpConnectionManager.cpp |  4 ++--
 4 files changed, 51 insertions(+), 5 deletions(-)

diff --git a/RawBridge.hpp b/RawBridge.hpp
index 9206faa..7a1255e 100644
--- a/RawBridge.hpp
+++ b/RawBridge.hpp
@@ -36,6 +36,16 @@ private:
   ip::tcp::socket serverSocket;
   ip::tcp::endpoint destination;
 
+#if BOOST_VERSION >= 107000
+  const boost::asio::executor &executor;
+
+  RawBridge(boost::shared_ptr<ip::tcp::socket> clientSocket,
+	    ip::tcp::endpoint& destination,
+	    const boost::asio::executor & executor) :
+    clientSocket(clientSocket), serverSocket(executor),
+    executor(executor), destination(destination), closed(0)
+  {}
+#else
   boost::asio::io_service &io_service;
 
   RawBridge(boost::shared_ptr<ip::tcp::socket> clientSocket,
@@ -44,6 +54,7 @@ private:
     clientSocket(clientSocket), serverSocket(io_service), 
     io_service(io_service), destination(destination), closed(0)
   {}
+#endif
 
   void handleConnect(Bridge::ptr bridge, const boost::system::error_code &error) {
     if (!error) Bridge::shuttle(&(*clientSocket), &serverSocket);
@@ -55,6 +66,15 @@ protected:
 
 public:
 
+#if BOOST_VERSION >= 107000
+  static ptr create(boost::shared_ptr<ip::tcp::socket> clientSocket,
+		    ip::tcp::endpoint& destination,
+		    const boost::asio::executor & executor)
+
+  {
+    return ptr(new RawBridge(clientSocket, destination, executor));
+  }
+#else
   static ptr create(boost::shared_ptr<ip::tcp::socket> clientSocket,
 		    ip::tcp::endpoint& destination,
 		    boost::asio::io_service & io_service)
@@ -62,6 +82,7 @@ public:
   {
     return ptr(new RawBridge(clientSocket, destination, io_service));    
   }
+#endif
 
   virtual ip::tcp::socket& getClientSocket() {
     return *clientSocket;
diff --git a/SSLConnectionManager.cpp b/SSLConnectionManager.cpp
index 9beed10..6087f65 100644
--- a/SSLConnectionManager.cpp
+++ b/SSLConnectionManager.cpp
@@ -44,7 +44,7 @@ SSLConnectionManager::SSLConnectionManager(io_service &io_service,
 }
 
 void SSLConnectionManager::acceptIncomingConnection() {
-  boost::shared_ptr<ip::tcp::socket> socket(new ip::tcp::socket(acceptor.get_io_service()));
+  boost::shared_ptr<ip::tcp::socket> socket(new ip::tcp::socket(acceptor.get_executor()));
 
   acceptor.async_accept(*socket, boost::bind(&SSLConnectionManager::handleClientConnection,
 					     this, socket, placeholders::error));
@@ -76,7 +76,7 @@ void SSLConnectionManager::shuttleConnection(boost::shared_ptr<ip::tcp::socket>
 					     ip::tcp::endpoint &destination)
 
 {
-  Bridge::ptr bridge = RawBridge::create(clientSocket, destination, acceptor.get_io_service());
+  Bridge::ptr bridge = RawBridge::create(clientSocket, destination, acceptor.get_executor());
   bridge->shuttle();
 }
 
@@ -134,7 +134,7 @@ void SSLConnectionManager::interceptSSL(boost::shared_ptr<ip::tcp::socket> clien
 					ip::tcp::endpoint &destination,
 					bool wildcardOK)
 {
-  ip::tcp::socket serverSocket(acceptor.get_io_service());
+  ip::tcp::socket serverSocket(acceptor.get_executor());
   boost::system::error_code error;
   serverSocket.connect(destination, error);
 
diff --git a/http/HttpBridge.hpp b/http/HttpBridge.hpp
index 863db21..edcffa1 100644
--- a/http/HttpBridge.hpp
+++ b/http/HttpBridge.hpp
@@ -40,12 +40,21 @@ class HttpBridge : public Bridge {
 
 public:
 
+#if BOOST_VERSION >= 107000
+  static ptr create(boost::shared_ptr<ip::tcp::socket> clientSocket,
+		    const executor& executor, 
+		    HttpBridgeListener *listener) 
+  {
+    return ptr(new HttpBridge(clientSocket, executor, listener));
+  }
+#else
   static ptr create(boost::shared_ptr<ip::tcp::socket> clientSocket,
 		    io_service& io_service, 
 		    HttpBridgeListener *listener) 
   {
     return ptr(new HttpBridge(clientSocket, io_service, listener));
   }
+#endif
 
   virtual ip::tcp::socket& getClientSocket() {
     return *clientSocket;
@@ -63,7 +72,11 @@ protected:
 
 private:
   int closed;
+#if BOOST_VERSION >= 107000
+  const executor& executor_;
+#else
   io_service& io_service_;
+#endif
   boost::shared_ptr<ip::tcp::socket> clientSocket;
   ip::tcp::socket serverSocket;
   
@@ -71,6 +84,17 @@ private:
   HttpHeaders headers;
   HttpBridgeListener *listener;
 
+#if BOOST_VERSION >= 107000
+  HttpBridge(boost::shared_ptr<ip::tcp::socket> clientSocket,
+	     const executor& executor, HttpBridgeListener *listener)
+    : clientSocket(clientSocket),
+      serverSocket(executor),
+      executor_(executor),
+      state(READING_HEADERS) 
+  {
+    this->listener = listener;
+  }
+#else
   HttpBridge(boost::shared_ptr<ip::tcp::socket> clientSocket,
 	     io_service& io_service, HttpBridgeListener *listener)
     : clientSocket(clientSocket),
@@ -80,6 +104,7 @@ private:
   {
     this->listener = listener;
   }
+#endif
 
   void setFinishedWithHeaders();  
 };
diff --git a/http/HttpConnectionManager.cpp b/http/HttpConnectionManager.cpp
index 9b1066c..8768bc8 100644
--- a/http/HttpConnectionManager.cpp
+++ b/http/HttpConnectionManager.cpp
@@ -53,7 +53,7 @@ HttpConnectionManager::HttpConnectionManager(io_service& io_service, int port,
 }
 
 void HttpConnectionManager::acceptIncomingConnection() {
-  boost::shared_ptr<ip::tcp::socket> socket(new ip::tcp::socket(acceptor_.get_io_service()));
+  boost::shared_ptr<ip::tcp::socket> socket(new ip::tcp::socket(acceptor_.get_executor()));
 
   acceptor_.async_accept(*socket, boost::bind(&HttpConnectionManager::handleClientConnection,
 					      this, socket, placeholders::error));
@@ -63,7 +63,7 @@ void HttpConnectionManager::acceptIncomingConnection() {
 void HttpConnectionManager::bridgeHttpRequest(boost::shared_ptr<ip::tcp::socket> socket,
 					      ip::tcp::endpoint destination)
 {
-  Bridge::ptr bridge = HttpBridge::create(socket, acceptor_.get_io_service(), 
+  Bridge::ptr bridge = HttpBridge::create(socket, acceptor_.get_executor(),
 					  FingerprintManager::getInstance());
   
   bridge->getServerSocket().
